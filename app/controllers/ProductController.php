<?php
/**
 * Main class to retrieve product data
 */
namespace App\Controllers;

use App\Models\Product;

class ProductController
{
    private $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function getProductName($id)
    {
        $name = $this->model->getProductName($id);
        $name = $name[0];

        $type = $this->model->getProductType($id);

        if ($type[0] == 'CD/DVD') {
            $size = $this->model->getProductAttribute($id, 'cd_size');
            $name .= ' ' . $size[0] . 'MB';
        }

        return $name;
    }

    public function getProductAttribute($id, $attribute)
    {
        $attribute_result = $this->model->getProductAttribute($id, $attribute);
        $name = $this->model->getProductName($id);
        $result = $name[0] . ' attribute ' . $attribute . ' = ' . $attribute_result[0];

        return $result;
    }

    public function getAllProductAttribute($id)
    {
        $attributes = $this->model->getAllProductAttributes($id);
        $price = $this->model->getProductPrice($id);
        $result = 'Product attributes: ';
        foreach ($attributes as $attribute){
            $result .= $attribute['attribute_name'] . ' - ' . $attribute['attribute_value'] . ', ';
        }
        $result .= 'Price: ' . $price[0];

        return trim($result);
    }
}