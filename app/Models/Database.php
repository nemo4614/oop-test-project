<?php

namespace App\Models;


class Database
{
    private $host      = 'localhost';
    private $user      = 'root';
    private $pass      = '';
    private $dbname    = 'products_db';

    private $dbh;
    private $error;

    public function __construct(){
        // Set DSN
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

        // Create a new PDO instanace
        try{
            $this->dbh = new \PDO($dsn, $this->user, $this->pass);
        }
            // Catch any errors
        catch(PDOException $e){
            $this->error = $e->getMessage();
        }
    }

    public function closeConnection(){
        $this->dbh = null;
    }

    public function getInstance()
    {
        return $this->dbh;
    }
}