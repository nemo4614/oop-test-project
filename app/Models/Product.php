<?php

namespace App\Models;

class Product
{
    public $dbh;

    public function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    public function getProductName($id)
    {
        $query = "SELECT `title` FROM `product` WHERE id = {$id}";

        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $row = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        return $row;
    }

    public function getProductAttribute($id, $attribute)
    {
        $query = "SELECT `attribute_value` FROM `attribute` attr
        LEFT JOIN `product_attribute` pr_attr ON attr.id = pr_attr.attribute_id
        LEFT JOIN `product` pr ON pr.id = pr_attr.product_id
        WHERE pr.id={$id} AND attr.attribute_name = '{$attribute}'";

        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $row = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        return $row;
    }

    public function getAllProductAttributes($id)
    {
        $query = "SELECT attr.`attribute_name`, attr.`attribute_value` FROM `attribute` attr
        LEFT JOIN `product_attribute` pr_attr ON attr.id = pr_attr.attribute_id
        LEFT JOIN `product` pr ON pr.id=pr_attr.product_id
        WHERE pr.id={$id}";

        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $row = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $row;
    }

    public function getProductType($id)
    {
        $query = "SELECT `type_value` FROM `product_types` pr_type
        LEFT JOIN `product` pr ON pr.id = pr_type.id
        WHERE pr.id={$id}";

        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $row = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        return $row;
    }

    public function getProductPrice($id)
    {
        $query = "SELECT `price` FROM `product`
        WHERE `product`.id={$id}";

        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $row = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        return $row;
    }

}