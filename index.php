<?php

require __DIR__ . '/vendor/autoload.php';

use App\Models\Database;
use \App\Models\Product;
use \App\Controllers\ProductController;

$db = new Database();

$product_model = new Product($db->getInstance());

$table_product = new ProductController($product_model);

var_dump($table_product->getProductName(1));

var_dump($table_product->getProductAttribute(1, 'material'));

var_dump($table_product->getAllProductAttribute(1));

$cd_product = new ProductController($product_model);

var_dump($table_product->getProductName(2));

var_dump($table_product->getProductAttribute(2, 'cd_size'));

var_dump($table_product->getAllProductAttribute(2));

$db->closeConnection();
